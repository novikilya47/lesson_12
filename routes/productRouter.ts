import * as express from 'express'
import { createProduct, deleteProduct, updateProduct, getAllProducts, getProductById } from '../controllers/productController.js';
import { checkRole } from '../controllers/authController.js';

const productRouter : any = express.Router();

productRouter.post("/create", checkRole, createProduct);
productRouter.delete("/delete", checkRole, deleteProduct);
productRouter.put("/update", checkRole, updateProduct);
productRouter.get("/allProducts", getAllProducts);
productRouter.get("/getById", getProductById);

export default productRouter;