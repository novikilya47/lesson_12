import * as express from 'express'
import { createCart, addProductToCart, getCartById } from '../controllers/cartController.js';
import { authJWT } from '../controllers/authController.js';

const cartRouter : any = express.Router();

cartRouter.post("/create", authJWT, createCart);
cartRouter.post("/addProduct", authJWT, addProductToCart);
cartRouter.get("/getCartById", authJWT, getCartById);

export default cartRouter;