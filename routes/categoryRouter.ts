import * as express from 'express'
const categoryRouter : any = express.Router();
import { checkRole } from '../controllers/authController.js';
import { createCategory, deleteCategory, updateCategory, getCategoryByID, getAllCategories } from "../controllers/categoryController.js";

categoryRouter.post("/create", checkRole,  createCategory);
categoryRouter.delete("/delete", checkRole,  deleteCategory);
categoryRouter.put("/update", checkRole,  updateCategory);
categoryRouter.get("/getByID", getCategoryByID);
categoryRouter.get("/getAll", getAllCategories);

export default categoryRouter;