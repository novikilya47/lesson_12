import * as express from 'express'
import { login, registration, createGoogleUrl, getToken } from "../controllers/authController.js";
import userRouter from "./userRouter.js";

const authRouter : any = express.Router();

authRouter.post('/registration', registration);
authRouter.post('/login', login, userRouter);
authRouter.get("/google", createGoogleUrl);
authRouter.get("/google/gettoken", getToken );

export default authRouter;