import * as mongoose from 'mongoose';
import {IUser} from '../models/userModel';
import { IProduct } from './productModel';

interface ICart extends mongoose.Document {
    user: IUser;
    products: Array<IProduct>;
  }

const cartSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: true},
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: "product", autopopulate: true}]
}, { versionKey: false });

cartSchema.plugin(require('mongoose-autopopulate'));
const Cart = mongoose.model<ICart>('cart', cartSchema);

export { Cart, ICart };