import * as mongoose from 'mongoose';
import { IProduct } from './productModel';
import { IUser } from './userModel';

interface IOrder extends mongoose.Document {
    number: number;
    user: IUser;
    products: Array<IProduct>;  
  }

const orderSchema = new mongoose.Schema({
    number:{ type: Number, unique: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user", autopopulate: true},
    products:[{ type: mongoose.Schema.Types.ObjectId, ref: "product", autopopulate: true}]
}, { versionKey: false });

orderSchema.plugin(require('mongoose-autopopulate'));
const Order = mongoose.model<IOrder>('order', orderSchema);

export { Order, IOrder};