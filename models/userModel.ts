import * as mongoose from 'mongoose';
import {ICart} from '../models/cartModel';
import {IOrder} from '../models/orderModel';

interface IUser extends mongoose.Document {
    userName: string;
    userSurname:string;
    userPassword:string;
    userEmail:string;
    userRole:string;
    userCart:ICart;
    userOrders:Array<IOrder>;
    cart: any
  }
 
const userSchema = new mongoose.Schema({
    userName: String, 
    userSurname: String, 
    userPassword: String,
    userEmail: {type: String, unique : true},
    userRole: {type : String, default : "user"}, 
    userCart: { type: mongoose.Schema.Types.ObjectId, ref: "cart", autopopulate: true},
    userOrders: [{ type: mongoose.Schema.Types.ObjectId, ref: "order", autopopulate: true}]
}, { versionKey: false });

userSchema.plugin(require('mongoose-autopopulate'));
const User = mongoose.model<IUser>("user", userSchema); 

export { User, IUser};