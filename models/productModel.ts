import * as mongoose from 'mongoose';
import { ICategory } from './categoryModel';

interface IProduct extends mongoose.Document {
    name: string;
    price: number;
    category: string;
  }

const productSchema = new mongoose.Schema({
    name: { type: String, require: true },
    price: { type: Number, default: 10 },
    category: { type: String, require: true }
}, { versionKey: false });

productSchema.plugin(require('mongoose-autopopulate'));
const Product = mongoose.model<IProduct>('product', productSchema);

export { Product, IProduct };