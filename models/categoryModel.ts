import * as mongoose from "mongoose";
import{IProduct} from "../models/productModel";

interface ICategory extends mongoose.Document {
    categoryName: string;
    products: Array<IProduct>;
  }
 
const categorySchema = new mongoose.Schema({
    categoryName: String,
    products:[{ type: mongoose.Schema.Types.ObjectId, ref: "product", autopopulate: true}]
}, { versionKey: false });

categorySchema.plugin(require('mongoose-autopopulate'));
const Category = mongoose.model<ICategory>("category", categorySchema); 

export { Category, ICategory };