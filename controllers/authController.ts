import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken';
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';
import { User, IUser } from "../models/userModel.js";
import { google } from 'googleapis';
import urlParse = require('url-parse');
import * as queryParse from 'query-string';
import axios from 'axios';

interface CustomRequest<T> extends Request {
    body: T;
  }

export const registration = async function (req:CustomRequest<IUser>, res:Response) {
    try{
        if (!/\S+@\S+\.\S+/.test(req.body.userEmail) || !req.body.userEmail.trim()) {
            res.status(400).send(`Неверный формат почты`);
        } else {
            const user = await User.findOne({ userEmail: req.body.userEmail });
            if (user) {
                res.status(400).send(`Данный пользователь уже зарегестрирован`);
            } else {
                User.create({ userName: req.body.userName, userSurname: req.body.userSurname, userEmail: req.body.userEmail, userPassword: req.body.userPassword, userRole: req.body.userRole }, function (err, doc) {
                    if (err) { return res.status(400).send(err); }
                    res.send(`Новый пользователь зарегистрирован ${doc}`);
                });
            }
        }
    } catch(e){
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const login = async function (req:CustomRequest<IUser>, res:Response) {
    try{
        const user = await User.findOne({ userEmail: req.body.userEmail, userPassword: req.body.userPassword });
        if (user) {
            const accessToken = jwt.sign({ userEmail: user.userEmail, userRole: user.userRole, _id: user._id, userName: user.userName }, accessTokenSecret);
            res.json({ accessToken });
        } else {
            res.status(400).send(`Неверные данные пользователя`);
        }
    }catch(e){
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const authJWT = function (req:CustomRequest<IUser>, res:Response, next:NextFunction) {
    try{
        const authHeader = req.headers.authorization;
        if (authHeader) {
            const token = authHeader.split(' ')[1];
            jwt.verify(token, accessTokenSecret, (err: any, user) => {
                if (err) {
                    return res.status(400).send("Неверный токен");
                }
                req.user = user as IUser;
                next();
            });
        } else {
            res.status(400).send("Не проведена авторизация");
        }
    }catch(e){
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const checkRole = function (req:CustomRequest<IUser>, res:Response, next:NextFunction) {
    try{
        if (req.user.userRole !== "admin") {
            return res.status(400).send("Доступ запрещен");
        }
        next();
    }catch(e){
        return res.status(500).send('Неверные параметры запроса');
    }
};

const clientId = '148208100073-l3edb2hf9qph89auihh7k3mc8l328men.apps.googleusercontent.com';
const clientSecret = 's7U7nDEt2LaX0XeqlrENgZP2';
const redirect = 'http://localhost:3000/auth/google/gettoken';

const oauth2Client = new google.auth.OAuth2(
    clientId,
    clientSecret,
    redirect
);

export const createGoogleUrl = async (req:Request, res:Response) => {
    try {
        const url = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: ['https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email'
            ]
        });
        return res.send({ url });
    } catch (e) {
        return res.status(403).send('Не удалось получить URL.');
    }
};

export const getToken = async (req:CustomRequest<IUser>, res: Response) => {
    try {
        const queryURL : any = new urlParse(req.url);
        const code : any = queryParse.parse(queryURL.query).code;
        const { tokens } = await oauth2Client.getToken(code);
        const { data } = await axios({
            url: 'https://www.googleapis.com/oauth2/v2/userinfo',
            method: 'get',
            headers: {
                authorization: `Bearer ${tokens.access_token}`
            }
        });


        let user = await User.findOne({ userEmail: data.email });
        if (!user) {
            const newUser = new User({ userName: data.given_name, userSurname: data.family_name, userEmail: data.email });
            await newUser.save();
            const accessToken = jwt.sign({ userName: newUser.userName, userRole: newUser.userRole, _id: newUser._id }, accessTokenSecret, { expiresIn: '24h' });
            return res.send(`Зарегестрирован новый пользователь: ${newUser.userName} ${newUser.userSurname}, \naccessToken: ${accessToken}`);
        } else if (user) {
            const accessToken = jwt.sign({ userName: user.userName, userRole: user.userRole, _id: user._id }, accessTokenSecret, { expiresIn: '24h' });
            return res.send(`accessToken: ${accessToken}`);
        } else {
            return res.status(403).send('Не удалось получить токен.');
        }
    } catch (e) {
        return res.status(500).send('Данный аккаунт уже внесён в БД.');
    }
};