import { User, IUser } from "../models/userModel.js";
import { Request, Response } from 'express'
import * as jwt from 'jsonwebtoken';
import * as JsonWebTokenError from "jsonwebtoken";
const accessTokenSecret = 'hfrjjfrjefrkkefrkldeko3jfjnrrf';

interface CustomRequest<T> extends Request {
    body: T;
  }

export const createUser = function (req:CustomRequest<IUser>, res:Response) {
    try {
        User.create({ userName: req.body.userName, userSurname: req.body.userSurname, userEmail: req.body.userEmail, userPassword: req.body.userPassword }, function (err, doc) {
            if (err) { return res.status(400).send(err); }
            res.send(`Пользователь создан ${doc}`);
        });
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const deleteUser = function (req:CustomRequest<IUser>, res:Response) {
    try {
        User.deleteOne({ userEmail: req.body.userEmail });
        res.send(`Пользователь удален`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const updateUser = function (req:CustomRequest<IUser & {newName:any}>, res:Response) {
    try {
        if (req.user.userRole == "admin") {
            User.updateOne({userEmail: req.body.userEmail}, { userName: req.body.newName });
            res.send(`Имя пользователя обновлено`);
        } else {
            User.updateOne({ userName: req.user.userName }, { userName: req.body.newName });
            res.send(`Имя пользователя обновлено`);
        }
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const getUserByID = function (req:CustomRequest<IUser>, res:Response) {
    try {
        User.findById({ _id: req.body._id }, function (err: any, doc: any) {
            if (err) { return res.status(400).send(err); }
            res.send(`Данные получены: ${doc}`);
        });
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const getAllUsers = function (req:CustomRequest<IUser>, res:Response) {
    try {
        User.find({}, function (err, doc) {
            if (err) { return res.status(400).send(err); }
            res.send(`Данные получены: ${doc}`);
        });
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};