import { Category, ICategory } from '../models/categoryModel.js';
import { Request, Response } from 'express'

interface CustomRequest<T> extends Request {
    body: T
 }

export const createCategory = function (req:CustomRequest<ICategory>, res:Response) {
    try {
        Category.create({ categoryName: req.body.categoryName}, function (err: any, doc: any) {
            if (err) { return res.status(400).send(err); }
            res.send(`Категория создана ${doc}`);
        });
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const deleteCategory = function (req:CustomRequest<ICategory>, res:Response) {
    try {
        Category.deleteOne({ _id: req.body._id});
        res.send(`Категория удалена`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const updateCategory = function (req:CustomRequest<ICategory & {newName : string}>, res:Response) {
    try {
        Category.updateOne({ _id: req.body._id}, { categoryName: req.body.newName });
        res.send(`Имя категории обновлено`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const getCategoryByID = function (req:CustomRequest<ICategory & {Id : string}>, res:Response) {
    try {
        Category.findById({ _id: req.body.Id }, function (err: any, doc: any) {
            if (err) { return res.status(400).send(err); }
            res.send(`Данные получены: ${doc}`);
        });
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const getAllCategories = function (req:CustomRequest<ICategory>, res:Response) {
    try {
        Category.find({}, function (err, doc) {
            if (err) { return res.status(400).send(err); }
            res.send(`Данные получены: ${doc}`);
        });
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

