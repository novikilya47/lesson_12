import { Request, Response } from 'express'
import { Cart, ICart } from '../models/cartModel.js';
import { User, IUser } from '../models/userModel.js';
import { Product, IProduct } from '../models/productModel.js';

interface CustomRequest <T> extends Request {
    body: T;
}

export const createCart = async (req: CustomRequest<IUser>, res:Response) => {
    let user = await User.findOne({ _id: req.user._id });

    if(!user){
        return res.status(400).send('User равен null.');
    }
    if (user.cart) {
        return res.status(400).send('У пользователя может быть только одна корзина.');
    } else {
        const newCart = await new Cart();
        newCart.user = req.user._id;
        await newCart.save();
        user.userCart = newCart._id;
        await user.save();
        return res.send(`Добавлена корзина. \nВладелец: ${newCart.user.userName} ${newCart.user.userSurname}.`);
    }
};

export const addProductToCart = async (req:CustomRequest<IProduct>, res:Response) => {
    try {

        
        
        let cartOwner = await User.findOne({ _id: req.user._id });
        if (!cartOwner) {
            return res.status(400).send('Не удалось найти корзину с таким id.');
        }
           let  cart = await Cart.findOne({ _id: cartOwner.userCart._id }),
                productName = await Product.findOne({ name: req.body.name });


        if (productName && cart) {
            cart.products.push(productName._id.toString());
            await cart.save();

            return res.send(`Товар: ${productName.name}, \nцена: ${productName.price} BYN 
                        добавлен в корзину покупателя ${cartOwner.userName} ${cartOwner.userSurname}.`);
        } else {
            return res.send('Отсутствует корзина покупок, либо указанный продукт не был найден.');
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса.');
    }
};

export const getCartById = async (req:CustomRequest<IUser>, res:Response) => {
    try {
        const cart = await Cart.findOne({ _id: req.body.id });

        return cart ? res.send(`Владельец искомой корзины: ${cart.user.userName} ${cart.user.userSurname}.`) 
            : res.send('Не удалось найти корзину с заданным id.');
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса.');
    }
};
