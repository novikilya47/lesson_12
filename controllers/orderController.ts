import { Cart, ICart } from '../models/cartModel.js';
import { User, IUser } from '../models/userModel.js';
import { Order, IOrder } from '../models/orderModel.js';
import { Request, Response } from 'express'

interface CustomRequest<T> extends Request {
    body: T;
  }

export const createOrder = async (req:CustomRequest<IOrder>, res:Response) => {
    let cart = await Cart.findOne({ _id: req.body.id }),
        user = await User.findOne({ _id: req.user._id });

    if (!cart) {
        return res.status(400).send('Не удалось найти корзину с таким id.');
    }

    let order = await Order.findOne({ number: req.body.number });
    if (order) {
        return res.status(400).send('Заказ с таким номером уже существует.');
    }
    
    const newOrder = new Order({ number: req.body.number });

    newOrder.user = req.user._id;
    await newOrder.save();
    newOrder.products = cart.products;
    await newOrder.save();
    if(!user){
        return res.status(400).send('User равен null.');
    }
    user.userOrders.push(newOrder._id);
    await user.save();
    await Cart.deleteOne({ _id: req.body.id });

    return res.send(`Создан новый заказ №${newOrder.number} \nОформлен пользователем ${user.userName} ${user.userSurname}. 
                Список товаров: ${newOrder.products}`);

};

export const deleteOrder = async (req:CustomRequest<IOrder>, res:Response) => {
    try {
        let order = await Order.findOne({ _id: req.body.id }),
            user = await User.findOne({ _id: req.user._id });

            if(!user){
                return res.status(400).send('User равен null.');
            }

        if (!order) {
            return res.send('Не удалось найти заказ с таким id.');
        } else if (user.userRole === "user" && order.user._id.toString() !== user._id.toString()) {
            return res.send('Недостаточно прав.');
        } else {
            Order.deleteOne({ _id: req.body.id });
            for (let i : any = 0; i < user.userOrders.length; i++) {
                if (user.userOrders[i]._id.toString() == order._id.toString()) {
                    user.userOrders.splice(user.userOrders.indexOf(i), 1);
                    await user.save();
                }
            }

            return res.send(`Заказ №${order.number} удалён.`);
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса. Не удалось найти заказ.');
    }
};

export const getOrderById = async (req:CustomRequest<IOrder>, res:Response) => {
    try {
        let order = await Order.findOne({ _id: req.body.id });

        return order ? res.send(`Заказ №${order.number}. \nОформлен пользователем: ${order.user.userName} ${order.user.userSurname}`)
            : res.status(400).send('Не удалось найти заказ с таким id.');
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса. Не удалось найти заказ.');
    }
};

export const getAllOrders = async (req:CustomRequest<IUser>, res:Response) => {
    try {
        let orders = await Order.find(),
            user = await User.findOne({ _id: req.user._id });

            if(!user){
                return res.status(400).send('User равен null.');
            }

        if (user.userRole === "admin" && orders) {
            return res.send(`Список заказов: ${orders}`);
        } else if (user.userRole === "user") {
            return res.send(`Ваш список заказов: ${user.userOrders}`);
        } else {
            return res.send(`У пользователя ${user.userName} ${user.userSurname} нет заказов.`);
        }
    } catch (e) {
        return res.status(400).send('Неверные параметры запроса.');
    }
};